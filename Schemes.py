import numpy as np
import matplotlib.pyplot as plt
import math


setup_A = {'b0': 2.5,         ### a high-end value of the coupling parameter
          'gamma': 0.75,      ### specifies the feedback of the thermocline gradient on the SST gradient
          'c': 1,             ### damping rate of SST anomalies
          'r': 0.25,          ### represents damping of the upper pcean heat content
          'alpha': 0.125,     ### relates enhanced easterly wind stress to the recharge of ocean heat content
          'miu': 2/3,         ### coupling coefficient
          'wc':  (3/32)**0.5, ### frequency
          'T0' : 1.125/7.5,    
          'h0' : 0/150,     
          'nt': 300}       
setup_D = {"miu0": 0.75,
           "miu_ann": 0.2,
           "fann": 0.02,
           "fran": 0.2,
           "tao_cor": 1/60,
           "dt":1/60
          }
### analytical solution
          
def ansol(nt,t,h0,T0,wc,dt,r,gamma,alpha,b):
    hw = np.empty(t*nt)
    TE = np.empty(t*nt)
    hw[0] = h0
    TE[0] = T0
    for i in range (t*nt-1):
        TE[i+1] = TE[0] * math.cos(wc*(i+1)*dt) + 1/wc*(r*TE[0] + gamma*hw[0])*math.sin(wc*(i+1)*dt)
        hw[i+1] = hw[0] * math.cos(wc*(i+1)*dt) - 1/wc*(r*hw[0] + alpha*b*TE[0])*math.sin(wc*(i+1)*dt)
    return (hw,TE)

def RK(nt,t,h0,T0,R,dt,r,gamma,alpha,b):
    hw = np.empty(t*nt)
    TE = np.empty(t*nt)
    hw[0] = h0
    TE[0] = T0
    for i in range (0,t*nt-1):
        k1_dhdt = - r * hw[i] - alpha * b * TE[i]
        k1_dTdt = R * TE[i] + gamma * hw[i]
        k2_dhdt = - r * (hw[i]+k1_dhdt*dt/2) - alpha * b * (TE[i]+k1_dTdt*dt/2)
        k2_dTdt = R * (TE[i]+k1_dTdt*dt/2) + gamma * (hw[i]+k1_dhdt*dt/2)
        k3_dhdt = - r * (hw[i]+k2_dhdt*dt/2) - alpha * b * (TE[i]+k2_dTdt*dt/2)
        k3_dTdt = R * (TE[i]+k2_dTdt*dt/2) + gamma * (hw[i]+k2_dhdt*dt/2)
        k4_dhdt =  - r * (hw[i]+k2_dhdt*dt) - alpha * b * (TE[i]+k2_dTdt*dt)
        k4_dTdt = R * (TE[i]+k3_dTdt*dt) + gamma * (hw[i]+k3_dhdt*dt)
        hw[i+1] = hw[i] + 1/6 * dt*(k1_dhdt + 2*k2_dhdt + 2*k3_dhdt + k4_dhdt)
        TE[i+1] = TE[i] + 1/6  * dt*(k1_dTdt + 2*k2_dTdt + 2*k3_dTdt + k4_dTdt)
    return(hw,TE)


def lf(nt,t,h0,T0,R,dt,r,gamma,alpha,b):
    hw = np.empty(t*nt)
    TE = np.empty(t*nt)
    hw[0] = h0
    TE[0] = T0
    hw[1] = hw[0] + (- r * hw[0] - alpha * b * TE[0])*dt
    TE[1] = TE[0] + (R * TE[0] + gamma * hw[0])*dt
    for i in range (1,t*nt-1):
        dhdt = - r * hw[i] - alpha * b * TE[i]
        dTdt = R * TE[i] + gamma * hw[i]
        hw[i+1] = hw[i-1] + 2 * dhdt * dt
        TE[i+1] = TE[i-1] + 2 * dTdt * dt
    return(hw,TE)


### leapfrog implementation with nonlinear

def lf_nl(nt,t,h0,T0,R,dt,r,gamma,alpha,b,en):
    hw = np.empty(t*nt)
    TE = np.empty(t*nt)
    hw[0] = h0
    TE[0] = T0
    hw[1] = hw[0] + (- r * hw[0] - alpha * b * TE[0])*dt
    TE[1] = TE[0] + (R * TE[0] + gamma * hw[0]-en*(hw[0]+b*TE[0])**3)*dt
    for i in range (1,t*nt-1):
        dhdt = - r * hw[i] - alpha * b * TE[i]
        dTdt = R * TE[i] + gamma * hw[i]-en*(hw[i]+b*TE[i])**3
        hw[i+1] = hw[i-1] + 2 * dhdt * dt
        TE[i+1] = TE[i-1] + 2 * dTdt * dt
    return(hw,TE)


### Runge-Kutta implementation with non-linear

def RK_nl(nt,t,h0,T0,R,dt,r,gamma,alpha,b,en):
    hw = np.empty(t*nt)
    TE = np.empty(t*nt)
    hw[0] = h0
    TE[0] = T0
    for i in range (0,t*nt-1):
        k1_dhdt = - r * hw[i] - alpha * b * TE[i]
        k1_dTdt = R * TE[i] + gamma * hw[i]-en*(hw[i]+b*TE[i])**3
        k2_dhdt = - r * (hw[i]+k1_dhdt*dt/2) - alpha * b * (TE[i]+k1_dTdt*dt/2)
        k2_dTdt = R * (TE[i]+k1_dTdt*dt/2) + gamma * (hw[i]+k1_dhdt*dt/2)-en*(hw[i]+k1_dhdt*dt/2+b*(TE[i]+k1_dTdt*dt/2))**3
        k3_dhdt = - r * (hw[i]+k2_dhdt*dt/2) - alpha * b * (TE[i]+k2_dTdt*dt/2)
        k3_dTdt = R * (TE[i]+k2_dTdt*dt/2) + gamma * (hw[i]+k2_dhdt*dt/2)-en*(hw[i]+k2_dhdt*dt/2+b*(TE[i]+k2_dTdt*dt/2))**3
        k4_dhdt =  - r * (hw[i]+k2_dhdt*dt) - alpha * b * (TE[i]+k2_dTdt*dt)
        k4_dTdt = R * (TE[i]+k3_dTdt*dt) + gamma * (hw[i]+k3_dhdt*dt)-en*(hw[i]+k3_dhdt*dt+b*(TE[i]+k3_dTdt*dt))**3
        hw[i+1] = hw[i] + 1/6 * dt*(k1_dhdt + 2*k2_dhdt + 2*k3_dhdt + k4_dhdt)
        TE[i+1] = TE[i] + 1/6  * dt*(k1_dTdt + 2*k2_dTdt + 2*k3_dTdt + k4_dTdt)
    return(hw,TE)


## leapfrog for "test self-excitation hypotheses"

def lf_seh(nt,t_p,h0,T0,R,dt,r,gamma,alpha,b,en,miu0,miu_ann,b0,c):
    tao = 6
    miu = np.empty(t_p*nt)
    for i in range(t_p*nt):
        miu[i] = miu0*(1+miu_ann*math.cos(2*np.pi*i*dt/tao-5*np.pi/6))
    b = b0 * miu
    R = gamma * b - c
    hw = np.empty(t_p*nt)
    TE = np.empty(t_p*nt)
    hw[0] = h0
    TE[0] = T0
    hw[1] = hw[0] + (- r * hw[0] - alpha * b[0] * TE[0])*dt
    TE[1] = TE[0] + (R[0] * TE[0] + gamma * hw[0]-en*(hw[0]+b[0]*TE[0])**3)*dt
    for i in range (1,t_p*nt-1):
        dhdt = - r * hw[i] - alpha * b[i] * TE[i]
        dTdt = R[i] * TE[i] + gamma * hw[i]-en*(hw[i]+b[0]*TE[i])**3
        hw[i+1] = hw[i-1] + 2 * dhdt * dt
        TE[i+1] = TE[i-1] + 2 * dTdt * dt
    return(hw,TE)


## Runge-Kutta for "test self-excitation hypotheses"

def RK_seh(nt,t_p,h0,T0,R,dt,r,gamma,alpha,b,en,miu0,miu_ann,b0,c):
    tao = 6
    miu = np.empty(t_p*nt)
    for i in range(t_p*nt):
        miu[i] = miu0*(1+miu_ann*math.cos(2*np.pi*i*dt/tao-5*np.pi/6))
    b = b0 * miu
    R = gamma * b - c
    hw = np.empty(t_p*nt)
    TE = np.empty(t_p*nt)
    hw[0] = h0
    TE[0] = T0
    for i in range (t_p*nt-1):
        k1_dhdt = - r * hw[i] - alpha * b[i] * TE[i]
        k1_dTdt = R[i] * TE[i] + gamma * hw[i]-en*(hw[i]+b[i]*TE[i])**3
        k2_dhdt = - r * (hw[i]+k1_dhdt*dt/2) - alpha * b[i] * (TE[i]+k1_dTdt*dt/2)
        k2_dTdt = R[i] * (TE[i]+k1_dTdt*dt/2) + gamma * (hw[i]+k1_dhdt*dt/2)-en*(hw[i]+k1_dhdt*dt/2+b[i]*(TE[i]+k1_dTdt*dt/2))**3
        k3_dhdt = - r * (hw[i]+k2_dhdt*dt/2) - alpha * b[i] * (TE[i]+k2_dTdt*dt/2)
        k3_dTdt = R[i] * (TE[i]+k2_dTdt*dt/2) + gamma * (hw[i]+k2_dhdt*dt/2)-en*(hw[i]+k2_dhdt*dt/2+b[i]*(TE[i]+k2_dTdt*dt/2))**3
        k4_dhdt = - r * (hw[i]+k3_dhdt*dt) - alpha * b[i] * (TE[i]+k3_dTdt*dt)
        k4_dTdt = R[i] * (TE[i]+k3_dTdt*dt) + gamma * (hw[i]+k3_dhdt*dt)-en*(hw[i]+k3_dhdt*dt+b[i]*(TE[i]+k3_dTdt*dt))**3
        hw[i+1] = hw[i] + 1/6 * dt*(k1_dhdt + 2*k2_dhdt + 2*k3_dhdt + k4_dhdt)
        TE[i+1] = TE[i] + 1/6 * dt*(k1_dTdt + 2*k2_dTdt + 2*k3_dTdt + k4_dTdt)
    return(hw,TE)


### Runge-Kutta implementation with " test stochastic initiation hypotheses"
import random

def RK_sih(nt,t_p,h0,T0,R,dt,r,gamma,alpha,b,en,miu0,miu_ann,b0,c,fann,fran,tao_cor):
    tao = 6
    miu = np.empty(t_p*nt)
    for i in range(t_p*nt):
        miu[i] = miu0*(1+miu_ann*math.cos(2*np.pi*i*dt/tao-5*np.pi/6))
    b = b0 * miu
    R = gamma * b - c
    
    hw = np.empty(t_p*nt)
    TE = np.empty(t_p*nt)
    hw[0] = h0
    TE[0] = T0
    for i in range (t_p*nt-1):
        k1_dhdt = - r * hw[i] - alpha * b[i] * TE[i] - alpha * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k1_dTdt = R[i] * TE[i] + gamma * hw[i]-en*(hw[i]+b[i] *TE[i])**3 + gamma * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k2_dhdt = - r * (hw[i]+k1_dhdt*dt/2) - alpha * b[i] * (TE[i]+k1_dTdt*dt/2)- alpha * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k2_dTdt = R[i] * (TE[i]+k1_dTdt*dt/2) + gamma * (hw[i]+k1_dhdt*dt/2)-en*(hw[i]+k1_dhdt*dt/2+b[i] *(TE[i]+k1_dTdt*dt/2))**3+ gamma * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k3_dhdt = - r * (hw[i]+k2_dhdt*dt/2) - alpha * b[i] * (TE[i]+k2_dTdt*dt/2)- alpha * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k3_dTdt = R[i] * (TE[i]+k2_dTdt*dt/2) + gamma * (hw[i]+k2_dhdt*dt/2)-en*(hw[i]+k2_dhdt*dt/2+b[i] *(TE[i]+k2_dTdt*dt/2))**3+ gamma * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k4_dhdt =  - r * (hw[i]+k3_dhdt*dt) - alpha * b[i] * (TE[i]+k3_dTdt*dt)- alpha * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        k4_dTdt = R[i] * (TE[i]+k3_dTdt*dt) + gamma * (hw[i]+k3_dhdt*dt)-en*(hw[i]+k3_dhdt*dt+b[i] *(TE[i]+k3_dTdt*dt))**3+ gamma * (fann*(np.cos(2*np.pi*i*dt/tao))+fran*random.randint(-1,1)*tao_cor/dt)
        hw[i+1] = hw[i] + 1/6 * dt*(k1_dhdt + 2*k2_dhdt + 2*k3_dhdt + k4_dhdt)
        TE[i+1] = TE[i] + 1/6 * dt*(k1_dTdt + 2*k2_dTdt + 2*k3_dTdt + k4_dTdt)
    return(hw,TE)